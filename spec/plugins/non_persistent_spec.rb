require 'spec_helper'

RSpec.describe PowerStencil::Plugins::Base do

  let(:project_path) { $TEST_PROJECT_PATH }

  subject do
    PowerStencil.config[:'project-path'] = project_path
    PowerStencil.command_line_manager.cmd_line_args = ['info', '--project-path', project_path]
    processors = PowerStencil.command_line_manager.send :processors_hash
    processors['info'].first
  end

end