require 'spec_helper'

RSpec.describe PowerStencil::Project::Base do

  around(:all) do |example|
    tmpdir = Dir::Tmpname.make_tmpname File.join(Dir.tmpdir, 'tests_rspec'), nil
    UniverseCompiler::Universe::Base.instance_eval do
      @universes = nil
    end
    described_class.create_project_tree tmpdir
    FileUtils.mkpath File.join(tmpdir, 'test_folder')
    example.metadata[:current_tmpdir] = tmpdir
    example.run
    FileUtils.remove_entry tmpdir
  end

  let(:start_from) do |example|
    example.metadata[:current_tmpdir]
  end

  subject do
    described_class.new search_from_path: start_from
  end

  context 'when there is a project tree correctly defined' do

    it 'should be possible to instantiate a project' do
      expect { subject }.not_to raise_error
    end

    context 'when in a subfolder' do

      let(:start_from) do |example|
        File.join example.metadata[:current_tmpdir], 'test_folder'
      end

      it 'should find the project tree' do
        expect(subject.started_from).to eq start_from
        expected_root = File.expand_path '..', start_from
        expect(subject.project_root).to eq expected_root
      end


    end

  end

  context 'when there is no project tree correctly defined' do

    around(:example) do |example|
      empty_dir = Dir::Tmpname.make_tmpname File.join(Dir.tmpdir, 'tests_rspec'), nil
      FileUtils.mkpath empty_dir
      example.metadata[:empty_dir] = empty_dir
      example.run
      FileUtils.remove_entry empty_dir
    end

    let(:start_from) {|example| example.metadata[:empty_dir]}

    it 'should not be possible to instantiate a project' do
      expect { subject }.to raise_error PowerStencil::Error
    end

  end

end