require 'spec_helper'

class TestDirectoryProcessor
  include PowerStencil::Utils::DirectoryProcessor
  include Climatic::Proxy
end


RSpec.describe PowerStencil::Utils::DirectoryProcessor do

  let(:source) { $TEST_PROJECT_PATH }

  subject do
    TestDirectoryProcessor.new
  end

  it 'should have a method to recursively "do things" on files' do
    expect(subject).to respond_to :process_directory
  end

  context 'when the source directory is invalid' do

    around(:example) do |example|
      tmpdir = Dir::Tmpname.make_tmpname File.join(Dir.tmpdir, 'tests_rspec'), nil
      example.metadata[:current_tmpdir] = tmpdir
      example.run
    end

    let(:invalid_sources) { [nil, '/non_existing_directory_or_man_you_have_no_luck'] }

    it 'should fail with an exception' do |example|
      invalid_sources.each do |invalid_source|
        expect do
          subject.process_directory source: invalid_source,
                                    destination: example.metadata[:current_tmpdir]
        end .to raise_error PowerStencil::Error
      end
    end

  end

  context 'when the destination directory is invalid' do

    let(:invalid_destinations) { [ nil, source] }

    it 'should fail with an exception' do |example|
      invalid_destinations.each do |invalid_destination|
        expect do
          subject.process_directory source: source,
                                    destination: invalid_destination
        end .to raise_error PowerStencil::Error
      end
    end

  end

  context 'when source and dest are ok' do

    let(:destination) { Dir::Tmpname.make_tmpname File.join(Dir.tmpdir, 'tests_rspec'), nil }

    it 'should run with no exception' do
      expect do
        subject.process_directory source: source,
                                  destination: destination,
                                  ignore_files_pattern: '.copy_ignore' do |src_file, dest_file|
          expect(File.exists? src_file).to be_truthy
          expect(File.exists? dest_file).to be_falsey
          expect(src_file).not_to include 'do_not_copy_this_file'
          expect(src_file).not_to include 'ignore'
        end
      end .not_to raise_error
    end
  end

end