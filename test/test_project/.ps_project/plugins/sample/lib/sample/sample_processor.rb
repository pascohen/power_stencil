module Sample

  class Processor

    include Climatic::Script::UnimplementedProcessor
    include Climatic::Proxy
    include PowerStencil::Project::Proxy


    def execute
      puts_and_logs 'SAMPLE PROC WAZ HERE'
    end


  end


end