require 'sample/sample_processor'


module PowerStencil::Plugin::Sample

  VERSION = '0.0.1'.freeze
  REQUIRED_PLUGINS = []

  def self.setup_processors
    clm = PowerStencil.command_line_manager
    clm.register_processor clm.command_by_alias('sample'),
                           Sample::Processor.new
  end

  def self.setup_entity_definitions
    PowerStencil.logger.info 'Setting up sample plugin entities'
    require 'sample/entity_definitions/foo'
    require 'sample/entity_definitions/bar'
  end


  def self.setup_plugin_templates
    plugin_templates_path = File.expand_path File.join('..', '..', 'templates'), __FILE__
    Dir.entries(plugin_templates_path).reject { |e| %w(. ..).include? e }.each do |entry|
      template_path = File.join(plugin_templates_path, entry)
      if Dir.exist? template_path
        PowerStencil.project.register_template_path_for_type entry.to_sym, template_path
      end
    end
  end

  def self.post_build_hook(entity_to_build, files_path)
    unless %i(foo bar).include? entity_to_build.type
      raise PowerStencil::Error, "Sample plugin cannot build '#{entity_to_build.as_path}' !"
    end
    file_to_display = File.expand_path File.join(files_path, entity_to_build.file_to_display)
    puts File.read(file_to_display)

  end
end

# PowerStencil::Plugin::Sample = Sample