class BlockChain < PowerStencil::SystemEntityDefinitions::ProjectEntity

  entity_type :block_chain

  has_one :block_chain, name: :parent
  has_one :foo

  def root?
    fields.parent.nil?
  end

end

