# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'power_stencil/version'

Gem::Specification.new do |spec|
  spec.name          = 'power_stencil'
  spec.version       = PowerStencil::VERSION
  spec.authors       = ['Laurent B.']
  spec.email         = ['lbnetid+rb@gmail.com']

  spec.summary       = %q{PowerStencil is the Swiss-army knife templating workflow for developers and ops.}
  spec.description   = %q{PowerStencil is the Swiss-army knife templating workflow for developers and ops.}
  spec.homepage      = 'https://gitlab.com/tools4devops/power_stencil'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'

  spec.add_dependency 'climatic', '~> 0.2', '>= 0.2.26'
  spec.add_dependency 'dir_glob_ignore', '~> 0.3'
  spec.add_dependency 'universe_compiler', '~> 0.2', '>= 0.2.11'
  spec.add_dependency 'pry'

  spec.post_install_message = %Q{
Thank you for installing PowerStencil #{PowerStencil::VERSION} !
From the command line you can run `power_stencil --help`
If your shell is not completing the command:
  If you use rbenv: `rbenv rehash`
  If you use zsh  : `rehash`

Full documentation here   : #{spec.homepage}/blob/master/README.md
Feel free to report issues: #{spec.homepage}/issues
  }
end
