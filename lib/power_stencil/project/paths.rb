module PowerStencil
  module Project

    module Paths

      include PowerStencil::Utils::FileHelper

      attr_reader :project_config_root, :started_from

      def self.system_templates_path
        File.expand_path File.join('..', '..', '..', '..', 'etc', 'templates'), __FILE__
      end

      def self.project_system_template_path
        File.join system_templates_path, 'project'
      end

      def build_run_path(seed)
        File.join build_root_path, timestamped_uniq_dir(seed, Time.now)
      end

      def build_root_path
        File.join project_root, PowerStencil.config[:project_build_root_directory_name]
      end

      def template_path(entity_type)
        File.join PowerStencil::Project::Paths.system_templates_path, entity_type.to_s
      end

      def project_plugins_path
        File.join project_config_root, PowerStencil.config[:project_plugins_directory_name]
      end

      def project_templates_path
        File.join project_config_root, PowerStencil.config[:project_templates_directory_name]
      end

      def project_entity_definitions_path
        File.join project_config_root, PowerStencil.config[:project_entity_definitions_directory_name]
      end

      def project_plugin_path(plugin_name)
        File.join project_plugins_path, plugin_name
      end


      def plugin_commands_line_definition_file(plugin_name)
        File.join project_plugin_path(plugin_name), 'etc', 'command_line.yaml'
      end

      def plugin_config_specific_file(plugin_name)
        File.join project_plugin_path(plugin_name), 'etc', 'plugin_config.yaml'
      end

      def plugin_processors_dir(plugin_name)
        File.join project_plugin_path(plugin_name), 'lib', plugin_name, 'processors'
      end

      def plugin_entities_definitions_dir(plugin_name)
        File.join project_plugin_path(plugin_name), 'etc',  plugin_name, 'entities_definitions'
      end

      def project_entity_path(entity)
        File.join project_entities_path, entity.type.to_s, "#{entity.name}.yaml"
      end

      def user_entity_path(entity)
        File.join user_entities_path, entity.type.to_s, "#{entity.name}.yaml"
      end

      def project_entities_path
        File.join project_config_root, PowerStencil.config[:project_entities_directory_name]
      end

      def user_entities_path
        File.join project_config_root, PowerStencil.config[:user_entities_directory_name]
      end

      def project_root_setup?
        not project_config_root.nil?
      end

      def project_root
        File.expand_path '..', project_config_root
      end

      def project_versioned_config_file
        File.join project_config_root, PowerStencil.config[:versioned_project_config_file_name]
      end

      def project_personal_config_file
        File.join project_config_root, PowerStencil.config[:unversioned_user_project_config_file_name]
      end

      def config_directory_name
        @config_directory_name ||= PowerStencil.config[:default_config_directory_name]
      end

      def config_directory_name=(new_name)
        @config_directory_name = new_name
        initialize_paths started_from unless started_from.nil?
      end

      def initialize_paths(from_path)
        from_path = File.expand_path from_path
        @project_config_root = find_project_root from_path
        raise PowerStencil::Error, "Cannot find a project in '#{from_path}' !" unless project_root_setup?
        @started_from = project_config_root.nil? ? nil : from_path
        project_root
      end

      def find_project_root(from_path)
        find_recursively_in_path config_directory_name, from_path
      end

    end

  end
end