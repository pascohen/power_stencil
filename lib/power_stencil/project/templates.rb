module PowerStencil
  module Project

    module Templates

      def setup_templates_for_entities
        plugins.each do |plugin_name, plugin|
          logger.debug "Checking if plugin '#{plugin_name}' declares some templates..."
          plugin.register_plugin_templates
        end
      end

      def entity_type_templates
        @entity_type_templates ||= {}
      end

      def register_template_path_for_type(entity_type, path)
        if entity_type_templates.include? entity_type
          logger.warn "There is already a template-template path registered for entity type '#{entity_type}': '#{entity_type_templates[entity_type]}'."
        end
        raise PowerStencil::Error, "There is no template in path: '#{path}'" unless Dir.exist? path and File.readable? path
        raise PowerStencil::Error, "Trying to register a template for non existing type '#{entity_type}'" unless engine.available_entity_types.include? entity_type
        logger.debug "Registering '#{path}' as template for type '#{entity_type}'."
        entity_type_templates[entity_type] = path
      end

      def generate_entity_dir_for_entity(entity, force: false)
        unless entity_type_templates[entity.type].nil?
          logger.debug "Generating entity dir for entity '#{entity.as_path}'"
          target_path = File.join project_root, entity.type.to_s, entity.name.to_s
          render_entity_template_in entity, target_path, force: force
        end
      end

      def delete_entity_dir_for_entity(entity, force: false)
        return unless force
        unless entity_type_templates[entity.type].nil?
          logger.debug "Deleting entity files for entity '#{entity.as_path}'"
          target_path = File.join project_root, entity.type.to_s, entity.name.to_s
          FileUtils.rmtree target_path unless target_path.nil? or target_path.empty?
        end
      end

      private

      def render_entity_template_in(entity, entity_dir_path, force: false)
        entity_engine.render_source entity_type_templates[entity.type],
                                    entity_dir_path,
                                    overwrite_files: force,
                                    main_entry_point: entity.name
      end

    end

  end
end