module PowerStencil
  module Project

    module Config

      include PowerStencil::Project::Paths

      PROJECT_CONFIG_PRIORITY = 2000
      USER_CONFIG_PRIORITY = 2010
      PLUGIN_CONFIG_PRIORITY_MIN = 1000

      attr_reader :plugin_priority_count


      def load_project_specific_config
        # Optional config files should have less priority than the command line layer
        add_optional_config_layer project_versioned_config_file, 'versioned project config file', PROJECT_CONFIG_PRIORITY
        add_optional_config_layer project_personal_config_file, 'personal project config file', USER_CONFIG_PRIORITY
      end


      def add_plugin_config(plugin_name)
        yaml_file = plugin_config_specific_file plugin_name
        priority = if priority.nil?
                     PLUGIN_CONFIG_PRIORITY_MIN
                   else
                     plugin_priority_count + 1
                   end
        raise PowerStencil::Error, 'Too many plugins !!' if priority >= PROJECT_CONFIG_PRIORITY
        add_optional_config_layer yaml_file, "'#{plugin_name}' plugin specific config", priority
        @plugin_priority_count ||= 0
        @plugin_priority_count += 1
      end

      private

      def add_optional_config_layer(yaml_file, layer_name, layer_priority)
        if File.readable? yaml_file
          logger.debug "Loading #{layer_name}: '#{yaml_file}'..."
          begin
            new_config_layer = SuperStack::Layer.new.load yaml_file
            new_config_layer.name = layer_name
            new_config_layer.priority = layer_priority
            config << new_config_layer
            new_config_layer
          rescue => e
            logger.error "The #{layer_name} '#{yaml_file}' is invalid and has been ignored !"
            logger.debug PowerStencil::Error.report_error(e)
            false
          end
        else
          logger.debug "No #{layer_name} found."
          false
        end
      end

    end

  end
end
