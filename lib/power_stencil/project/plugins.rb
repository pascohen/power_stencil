module PowerStencil
  module Project

    module Plugins

      def plugins
        @plugins ||= {}
      end

      def create_plugin_tree(plugin_name, new_plugin_path, overwrite_files: false)
        raise PowerStencil::Error, "Plugin '#{plugin_name}' already exists !" if plugins.keys.include? plugin_name
        raise PowerStencil::Error, "Invalid plugin name '#{plugin_name}'" if (plugin_name.underscore =~ /^[_[:lower:]][_[:alnum:]]*$/).nil?
        entity_engine.dsl = PowerStencil::Dsl::PluginGeneration
        entity_engine.render_source entity_type_templates[:plugin_definition],
                                    new_plugin_path,
                                    overwrite_files: overwrite_files,
                                    main_entry_point: plugin_name

      end

      private

      def bootstrap_plugins
        initialize_gem_plugins
        initialize_local_plugins
        check_plugins_dependencies
        command_line_manager.definition_hash_to_commands
        plugins.each do |plugin_name, plugin|
          if plugin.capabilities[:processors]
            plugin.plugin_module.send PowerStencil::Plugins::Require::SETUP_PROCESSOR_CALLBACK
          end
        end
      end

      def check_plugins_dependencies
        plugins.each do |plugin_name, plugin|
          begin
            plugin.check_plugin_dependencies
          rescue PowerStencil::Error => pse
            PowerStencil.logger.debug pse.message
            PowerStencil.logger.warn "Discarding invalid plugin '#{plugin_name}'."
          end
        end
      end

      def initialize_gem_plugins
        # PowerStencil::logger.warn 'Gem plugins not yet supported ! Skipping...'
        if config[:project_plugins].empty?
          PowerStencil.logger.info "No gem plugin found in '#{project_plugins_path}'"
          return
        end
        config[:project_plugins].each do |plugin_definition|
          plugin_name, plugin_requirements = case plugin_definition
                                             when String
                                               [plugin_definition, '']
                                             when Hash
                                               [plugin_definition.keys.first, plugin_definition.values.first]
                                             end
          unless PowerStencil::Plugins::Base.gem_locally_installed? plugin_name, plugin_requirements
            PowerStencil::Plugins::Base.install_gem plugin_name, plugin_requirements
          end
        end
      end

      def initialize_local_plugins
        unless File.directory? project_plugins_path
          PowerStencil.logger.info "No local plugin found in '#{project_plugins_path}'"
          return
        end

        candidates = Dir.entries(project_plugins_path)
                         .select { |e| File.directory? File.join(project_plugins_path, e) }
                         .reject { |d| %w(. ..).include? d }
        @plugins = {}
        candidates.each do |candidate|
          begin
            raise PowerStencil::Error, "Plugin '#{candidate}' already exists !" unless plugins[candidate].nil?
            plugins[candidate] = PowerStencil::Plugins::Base.new(candidate, self)
          rescue PowerStencil::Error => pse
            PowerStencil.logger.debug pse.message
            PowerStencil.logger.warn "Discarding invalid plugin '#{candidate}'."
          end
        end
      end

    end

  end
end
