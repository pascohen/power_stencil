require 'fileutils'

module PowerStencil
  module Project

    module Create

      include Climatic::Proxy

      def create_project_tree(path, config_directory_name = PowerStencil.config[:default_config_directory_name])
        path = File.expand_path path
        config_path = File.join path, config_directory_name
        if Dir.exists? config_path
          raise PowerStencil::Error, "The directory '#{path}' already contains a PowerStencil project !" unless config[:force]
        end
        logger.info "Creating project in '#{path}'..."
        render_project_template_in(path)
      end

      private

      def render_project_template_in(new_project_config_path)
        engine = PowerStencil::Engine::InitEngine.new
        engine.render_source PowerStencil::Project::Paths.project_system_template_path, new_project_config_path
      end

    end

  end
end
