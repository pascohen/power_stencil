module PowerStencil
  module Project

    module Proxy

      def project
        PowerStencil.project
      end

    end

  end
end