require 'yaml'

module PowerStencil

  module Initializer

    BASE_COMMANDS_DEFINITION_FILE = 'base_commands_definition.yml'.freeze

    include Climatic::Proxy

    def bootstrap(cmd_line_args = ARGV.dup)
      setup_climatic cmd_line_args
      logger.debug 'Starting PowerStencil initialization...'
      setup_system_processors
      setup_universe_compiler_logger
      # @project = try_to_load_project fail_on_error: false
      begin
        project.setup_templates_for_entities # unless project.nil?
      rescue => e
        logger.debug PowerStencil::Error.report_error e
      end
      logger.debug 'PowerStencil initialization complete'
    end

    def project
      @project ||= try_to_load_project
    end

    private

    include PowerStencil::Utils::FileHelper

    def try_to_load_project(fail_on_error: true)
      PowerStencil::Project::Base.instantiate_from_config config

    end

    def setup_system_processors
      logger.debug 'Registering system processors'
      {
          '' => PowerStencil::CommandProcessors::Root,
          init: PowerStencil::CommandProcessors::Init,
          info: PowerStencil::CommandProcessors::Info,
          get: PowerStencil::CommandProcessors::Get,
          check: PowerStencil::CommandProcessors::Check,
          create: PowerStencil::CommandProcessors::Create,
          edit: PowerStencil::CommandProcessors::Edit,
          delete: PowerStencil::CommandProcessors::Delete,
          shell: PowerStencil::CommandProcessors::Shell,
          :'new-plugin' => PowerStencil::CommandProcessors::NewPlugin,
          build: PowerStencil::CommandProcessors::Build
      }.each do |command_name, processor|
        command_line_manager.register_processor command_line_manager.command_by_alias(command_name),
                                                processor.new
      end
    end

    def setup_climatic(cmd_line_args)
      mngr = Climatic::ConfigLayers::CommandLineLayer.build_command_line_manager base_commands_definition_file
      Climatic.bootstrap cmd_line_args: cmd_line_args, command_manager: mngr
      begin
        # Fix command line layer priority to allow a bigger number of plugins
        config.command_line_layer.priority = 999
        config.command_line_layer.reload
        config.include_gem_layer_for :power_stencil
      rescue Slop::UnknownOption => e
        # Forget those errors. Will be caught later.
      end

    end

    def setup_universe_compiler_logger
      UniverseCompiler.logger = Climatic.logger
    end

    def base_commands_definition_file
      File.join gempath, 'etc', BASE_COMMANDS_DEFINITION_FILE
    end

    def gempath
      Climatic::ConfigLayers::ExecutableGemLayer.executable_gem_config_root
    end


  end

end
