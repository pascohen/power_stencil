require 'power_stencil/dsl/entities'

module PowerStencil
  module Dsl

    class Base

      include PowerStencil::Project::Proxy
      include PowerStencil::Dsl::Entities

      attr_accessor :main_entry_point

      def initialize(universe)
        @universe = universe
      end

      def project_config
        project.config
      end

    end

  end
end