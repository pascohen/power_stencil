module PowerStencil
  module Dsl

    class PluginGeneration < PowerStencil::Dsl::Base

      def plugin_name
         main_entry_point.underscore
      end

      def plugin_module_name
        plugin_name.camelize
      end

    end

  end
end
