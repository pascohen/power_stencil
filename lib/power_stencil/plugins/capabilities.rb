module PowerStencil
  module Plugins

    module Capabilities

      DEFAULT_CAPABILITIES = {
          command_line: false,
          local_config: false,
          processors: false,
          build: false,
          dsl: false,
          entity_definitions: false,
          code: false,
          version: false,
          templates: false
      }.freeze

      def capabilities
        @capabilities ||= DEFAULT_CAPABILITIES.dup
      end

      def valid?
        capabilities.keys.each {|cap_name| return true if capabilities[cap_name]}
        false
      end

    end

  end
end