module PowerStencil
  module Plugins

    module Templates

      SETUP_PLUGIN_TEMPLATES = :setup_plugin_templates

      def register_plugin_templates
        if plugin_module.respond_to? SETUP_PLUGIN_TEMPLATES
          logger.debug "Plugin '#{name}' declares to have template(s)."
          capabilities[:templates] = true
          plugin_module.send SETUP_PLUGIN_TEMPLATES
        else
          logger.debug "Plugin '#{name}' declares to have no template."
        end
      end

    end

  end
end
