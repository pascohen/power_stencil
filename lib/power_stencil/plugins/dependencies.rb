module PowerStencil
  module Plugins

    module Dependencies

      def has_dependencies?
        not declared_dependencies.empty?
      end

      def declared_dependencies
        if plugin_module.constants.include? :REQUIRED_PLUGINS
          plugin_module::REQUIRED_PLUGINS
        else
          []
        end
      end

      def check_plugin_dependencies
        logger.info "Checking plugin '#{name}' dependencies !"
        declared_dependencies.each do |dependency|
          logger.debug "Checking dependency of plugin '#{name}' to plugin '#{dependency}'"
          unless project.plugins.keys.include? dependency.to_s
            raise PowerStencil::Error, "Unmatched dependency '#{dependency}' for plugin '#{name}' !"
          end
        end
        logger.debug "Dependencies for plugin '#{name}' are Ok."
      end

    end

  end
end
