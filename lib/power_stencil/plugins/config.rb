module PowerStencil
  module Plugins

    module Config

      attr_reader :plugin_specific_config

      def has_plugin_specific_config?
        yaml_file = project.plugin_config_specific_file(name)
        if File.exists? yaml_file and File.file? yaml_file and File.readable? yaml_file
          logger.info "Found plugin specific config to global config from '#{name}' plugin..."
          return true
        end
        logger.debug "There is no extra command line definition provided by plugin '#{name}'."
        false
      end

      def load_plugin_specific_config
        yaml_file = project.plugin_config_specific_file(name)
        if has_plugin_specific_config?
          project.add_plugin_config(name)
          capabilities[:local_config] = true
        end
      rescue => e
        logger.debug PowerStencil::Error.report_error(e)
        logger.warn "Could not load yaml file '#{yaml_file}' because '#{e.message}'"
      end

    end

  end
end
