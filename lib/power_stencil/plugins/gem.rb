require 'rubygems/commands/install_command'

module PowerStencil
  module Plugins

    module Gem

      NO_DOC = %w(--no-ri --no-rdoc).freeze

      def gem_locally_installed?(plugin_name, plugin_requirements)
        get_spec(plugin_name, plugin_requirements).nil? ? false : true
      end


      def install_gem(plugin_name, plugin_requirements)
        cmd = ::Gem::Commands::InstallCommand.new
        opts = [plugin_name]
        opts << plugin_requirements unless plugin_requirements.empty?
        opts.concat NO_DOC
        cmd.handle_options opts
        PowerStencil.logger.debug "Installing plugin '#{plugin_name}'..."
        cmd.execute
        PowerStencil.logger.debug "Plugin '#{plugin_name}' successfully installed."
      rescue ::Gem::SystemExitException => e
        PowerStencil.logger.warn PowerStencil::Error.report_error(e)
      end



      private

      def get_spec(plugin_name, plugin_requirements)
        candidates = ::Gem::Specification.find_all_by_name plugin_name
        if candidates.empty?
          PowerStencil.logger.debug "Could not find required plugin '#{plugin_name}'"
          return nil
        end

        req = if plugin_requirements.nil? or plugin_requirements.empty?
                ::Gem::Requirement::DefaultRequirement
              else
                ::Gem::Requirement.create plugin_requirements
              end

        selected = candidates.select {|candidate| req.satisfied_by? candidate.version}.sort {|a, b| a.version <=> b.version}
        if candidates.empty?
          PowerStencil.logger.debug "Could not find required plugin '#{plugin_name}'"
          return nil
        end
        selected.last

      end

    end

  end
end
