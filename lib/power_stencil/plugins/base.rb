require 'power_stencil/plugins/dependencies'
require 'power_stencil/plugins/config'
require 'power_stencil/plugins/command_line'
require 'power_stencil/plugins/templates'
require 'power_stencil/plugins/require'
require 'power_stencil/plugins/capabilities'
require 'power_stencil/plugins/gem'

module PowerStencil

  module Plugins

    class Base

      extend PowerStencil::Plugins::Gem

      include Climatic::Proxy
      include PowerStencil::Plugins::Dependencies
      include PowerStencil::Plugins::Config
      include PowerStencil::Plugins::CommandLine
      include PowerStencil::Plugins::Require
      include PowerStencil::Plugins::Capabilities
      include PowerStencil::Plugins::Templates

      attr_reader :name, :version, :entry_point_path

      def initialize(name, project)
        @name = name
        @project = project
        @version = PowerStencil::Utils::SemanticVersion.new '0.0.0-not-specified'
        logger.debug "Loading plugin '#{name}'..."
        setup_plugin
        logger.info "Plugin '#{name}' successfully available"
        logger.debug "Plugin '#{name}' has following capabilities: #{capabilities.inspect}"
      end

      def path
        project.project_plugin_path(name)
      end

      private

      attr_reader :project

      def setup_plugin
        require_entry_point
        load_plugin_specific_config
        load_yaml_command_definition
      end

    end

  end
end