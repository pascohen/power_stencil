module PowerStencil
  module Plugins

    module Require

      PLUGINS_NAMESPACE = PowerStencil::Plugin
      SETUP_PROCESSOR_CALLBACK = :setup_processors
      SETUP_DSL_CALLBACK = :setup_dsl
      SETUP_ENTITY_DEFINITIONS_CALLBACK = :setup_entity_definitions
      POST_BUILD_HOOK = :post_build_hook

      include PowerStencil::Utils::SecureRequire
      include PowerStencil::Utils::GemUtils

      def plugin_module
        if PLUGINS_NAMESPACE.constants.include? module_short_name
          PLUGINS_NAMESPACE.const_get module_short_name
        else
          module_name = if respond_to? :name
                          name.nil? ? 'Unknown' : module_short_name
                        else
                          __DIR__
                        end
          raise PowerStencil::Error, "Invalid plugin '#{module_name}'"
        end
      end

      private

      def module_short_name
        name.split(/[-_]+/).map(&:capitalize).join.to_sym
      end

      def require_entry_point
        if is_available_gem? name
          logger.debug "Plugin '#{name}' is actually a Ruby Gem."
          raise "Plugin (#{name}) provided as a Ruby gem is not yet supported !"
        else
          @entry_point_path = File.join project.project_plugin_path(name), 'lib', "#{name.underscore}.rb"
          logger.debug "Plugin '#{name}' is provided locally: '#{entry_point_path}'"
          plugin_root_path = File.dirname(entry_point_path)
          begin
            $LOAD_PATH << plugin_root_path
            securely_require entry_point_path do
              check_plugin_module_capabilities
            end
          rescue LoadError => e
            logger.warn "As plugin '#{name}' code is invalid, removing '#{plugin_root_path}' from LOAD_PATH"
            $LOAD_PATH.delete plugin_root_path
          end

        end
      end

      def check_plugin_module_capabilities
        unless plugin_module.nil?
          capabilities[:code] = true
          setup_version
        end
        capabilities[:processors] = plugin_module.respond_to? SETUP_PROCESSOR_CALLBACK
        capabilities[:dsl] = plugin_module.respond_to? SETUP_DSL_CALLBACK
        capabilities[:entity_definitions] = plugin_module.respond_to? SETUP_ENTITY_DEFINITIONS_CALLBACK
        capabilities[:build] = plugin_module.respond_to? POST_BUILD_HOOK
      end

      def setup_version
        @version = PowerStencil::Utils::SemanticVersion.new plugin_module::VERSION
        capabilities[:version] = true
        logger.debug "Plugin '#{name}' is in version: #{version}"
      rescue
        logger.warn "No version specified for plugin '#{name}'."
      end

    end

  end
end
