module PowerStencil
  module Plugins

    module CommandLine

      attr_reader :plugin_command_line_modifier

      private

      def load_yaml_command_definition
        yaml_file = project.plugin_commands_line_definition_file name
        if File.exists? yaml_file and File.file? yaml_file and File.readable? yaml_file
          logger.info "Adding extra command line definition for '#{name}' plugin..."
          @plugin_command_line_modifier = project.yaml_file_to_hash yaml_file
          command_line_manager.contribute_to_definition @plugin_command_line_modifier, layer_name: "Plugin: #{name}"
          capabilities[:command_line] = true
        else
          logger.debug "There is no extra command line definition provided by plugin '#{name}'."
        end
      rescue => e
        logger.debug PowerStencil::Error.report_error(e)
        logger.warn "Could not load yaml file '#{yaml_file}' because '#{e.message}'"
      end
    end

  end
end
