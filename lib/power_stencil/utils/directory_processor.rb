module PowerStencil
  module Utils

    module DirectoryProcessor

      def process_directory(source: nil,
                            destination: nil,
                            ignore_files_pattern: DirGlobIgnore::IgnoreFileLists::DEFAULT_FILE_NAME)
        raise PowerStencil::Error, 'Source and destination have to be different' if source == destination
        source = check_source_dir source
        destination = check_destination_dir destination
        # Dir.glob_with_ignore_file ignore_file, base_dir, *glob_args, &block
        file_pattern = "#{source}/**/*"
        res = {}
        Dir.glob_with_ignore_file ignore_files_pattern, source, file_pattern, File::FNM_DOTMATCH do |original_file|
          logger.debug "Processing '#{original_file}'"
          res[original_file] = destination_file(original_file, source, destination)
          if block_given?
            yield original_file, destination_file(original_file, source, destination)
          end
        end
        res
      end

      private

      def destination_file(original_file, source, destination)
        original_file.gsub /^#{source}/, destination
      end

      def check_source_dir(source)
        raise PowerStencil::Error, 'Please specify a source directory !' if source.nil?
        source = File.expand_path source
        raise PowerStencil::Error, "Source directory '#{source}' does not exist !" unless File.exist? source
        source
      end

      def check_destination_dir(destination)
        raise PowerStencil::Error, 'Please specify a destination directory !' if destination.nil?
        destination = File.expand_path destination
        if File.exist? destination
          raise PowerStencil::Error, "Destination '#{destination}' is not a directory !" unless File.directory? destination
        end
        # raise PowerStencil::Error, "Destination '#{destination}' is not a directory !" unless File.directory? destination
        # raise PowerStencil::Error, "Destination directory '#{destination}' already exists !" if File.exist? destination
        destination

      end


    end

  end
end