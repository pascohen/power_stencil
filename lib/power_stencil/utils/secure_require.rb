module PowerStencil
  module Utils

    module SecureRequire

      def securely_require(ruby_source_file, fail_on_error: false)
        PowerStencil.logger.debug "Securely requiring Ruby source file '#{ruby_source_file}'..."
        require ruby_source_file
        yield if block_given?
      rescue => e
        PowerStencil.logger.debug PowerStencil::Error.report_error(e)
        msg = "Could not require Ruby source file '#{ruby_source_file}' !"
        if fail_on_error
          msg << ' Aborting !'
          PowerStencil.logger.error msg
          raise PowerStencil::Error, "Invalid Ruby source file: '#{ruby_source_file}' !"
        else
          msg << " (#{e.message})"
          msg << ' Ignoring...'
          PowerStencil.logger.warn msg
        end
      end

    end
  end
end