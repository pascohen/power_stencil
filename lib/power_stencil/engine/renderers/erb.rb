module PowerStencil
  module Engine
    module Renderers

      module Erb

        private

        def render_erb_template(source, context)
          logger.debug "Using ERB to render file '#{source}'"
          ERB.new(File.read(source), nil, '-').result(context)
        rescue => e
          logger.debug PowerStencil::Error.report_error(e)
          raise PowerStencil::Error, "Error rendering '#{source}': '#{e.message}'"
        end

      end

    end
  end
end
