require 'power_stencil/engine/base'
require 'power_stencil/engine/entities_handling'
require 'power_stencil/engine/build_handling'

module PowerStencil
  module Engine

    class ProjectEngine < PowerStencil::Engine::Base

      include PowerStencil::Engine::EntitiesHandling
      include PowerStencil::Engine::BuildHandling

      attr_reader :project

      def initialize(project)
        super()
        @project = project
        load_system_entities_definition
        load_plugins_entities_definition
        load_project_entities_definition
        load_entities
      end

      protected

      def load_project_entities_definition
        dir = project.project_entity_definitions_path
        if Dir.exist? dir and File.readable? dir
          logger.info 'Loading project specific entity definitions.'
          $LOAD_PATH << dir
          require_definition_files [dir]
        end
      end

      def load_plugins_entities_definition
        project.plugins.each do |plugin_name, plugin|
          if plugin.capabilities[:entity_definitions]
            logger.info "Loading entity definitions for plugin '#{plugin_name}'."
            plugin.plugin_module.send PowerStencil::Plugins::Require::SETUP_ENTITY_DEFINITIONS_CALLBACK
          end
        end
      end

      def load_entities
        root_universe.clear
        load_system_entities
        load_project_entities
        load_user_entities
      end

      private

      def load_project_entities
        logger.debug 'Loading project entities - NOT IMPLEMENTED'
        root_universe.import project.project_entities_path, stop_on_error: false do |new_entity|
          process_entity new_entity
        end
      end

      def load_user_entities
        logger.debug 'Loading user entities - NOT IMPLEMENTED'
        root_universe.import project.user_entities_path, stop_on_error: false do |new_entity|
          process_entity new_entity
        end
      end

      def process_entity(entity)
        logger.debug "New entity: '#{entity.name}' (#{entity.type})"
      end


    end

  end
end
