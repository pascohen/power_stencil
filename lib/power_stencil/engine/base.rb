require 'power_stencil/engine/entities_definitions'
require 'power_stencil/engine/directory_processor'

require 'power_stencil/engine/renderers/erb'
require 'power_stencil/dsl/base'
require 'power_stencil/dsl/plugin_generation'

module PowerStencil
  module Engine

    class Base

      UNIVERSE_BASENAME = 'Project entities'.freeze

      include Climatic::Proxy
      include PowerStencil::Engine::EntitiesDefinition
      include PowerStencil::Engine::DirectoryProcessor

      include PowerStencil::Engine::Renderers::Erb

      attr_accessor :dsl
      attr_reader :root_universe

      def initialize(universe = UniverseCompiler::Universe::Base.new(unique_name))
        @root_universe = universe
        @dsl = PowerStencil::Dsl::Base
      end

      def running_context(universe = root_universe, main_entry_point: nil)
        context = dsl.new universe
        context.main_entry_point = main_entry_point
        context.instance_eval do
          binding
        end
      end

      protected

      def load_system_entities
        root_universe << PowerStencil::SystemEntityDefinitions::ProjectConfig.new(fields: PowerStencil.config[])
      end

      def unique_name
        '%s (%f)' % [UNIVERSE_BASENAME, Time.now.to_f]
      end

    end

  end
end