require 'power_stencil/engine/project_engine'

module PowerStencil
  module Engine

    class EntityEngine < PowerStencil::Engine::Base

      UNIVERSE_BASENAME = 'Entities directory creation'.freeze

      def initialize(universe = UniverseCompiler::Universe::Base.new(unique_name))
        super
        load_system_entities
      end


    end


  end
end