module PowerStencil
  module Engine

    module EntitiesDefinition

      SYSTEM_ENTITY_DEFINITION_ENTRY_POINT = 'power_stencil/system_entity_definitions/all'.freeze

      include PowerStencil::Utils::SecureRequire

      def require_definition_files(files_or_dirs)
        required_files = []
        files_or_dirs.each do |file_or_dir|
          if File.directory? file_or_dir and File.readable? file_or_dir
            Dir.entries(file_or_dir).grep(/\.rb$/).each do |file|
              required_files << File.join(file_or_dir, file)
            end
            next
          end
          if File.file? file_or_dir and File.readable? file_or_dir
            required_files << file_or_dir
            next
          end
          # This is a ruby library or there is something wrong
          securely_require file_or_dir
          # unless securely_require file_or_dir
          #   logger.warn "While trying to load definition files, found that '#{file_or_dir}' has a problem. Ignored..."
          # end
        end
        required_files.sort!.each {|file| securely_require file, fail_on_error: true}
        required_files
      end

      private

      def load_system_entities_definition
        require_definition_files [SYSTEM_ENTITY_DEFINITION_ENTRY_POINT]
      end

    end

  end
end
