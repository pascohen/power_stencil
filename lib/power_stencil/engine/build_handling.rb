module PowerStencil
  module Engine

    module BuildHandling

      include Climatic::Utils::SafeExec

      attr_reader :last_build_report

      def build(entities_to_build, fail_on_error: false, parallelized: false)
        logger.debug 'Starting build process...'
        raise PowerStencil::Error, 'Parallel builds are not yet supported !' if parallelized

        build_target_seed =  entities_to_build.map(&:as_path).map do |entity_full_name|
          entity_full_name.tr '/', '_'
        end .join '_'
        build_target_path = project.build_run_path build_target_seed

        @last_build_report = {}

        entities_to_build.each do |entity_to_build|
          begin
            entity_build_report = []
            unless entity_to_build.buildable?
              msg = "Entity '#{entity_to_build.as_path}' is not buildable !"
              entity_build_report << msg
              raise PowerStencil::Error, msg
            end
            build_entity_target_path = File.join build_target_path, entity_to_build.as_path.tr('/', '_')
            puts_and_logs "De-templating files for '#{entity_to_build.as_path}' into '#{build_entity_target_path}'"
            build_entity entity_to_build, build_entity_target_path
            entity_build_report << 'Ok'
          rescue => e
            logger.error "Failed building '#{entity_to_build.as_path}' because '#{e.message}' !"
            entity_build_report << e.message
            if fail_on_error
              raise e
            else
              logger.debug PowerStencil::Error.report_error e
            end
          ensure
            last_build_report[entity_to_build.as_path] = entity_build_report
          end
        end
      end

      private

      def build_entity(entity_to_build, target_path)
        logger.info "Building #{entity_to_build.as_path} in '#{target_path}'"
        render_source entity_to_build.templates_path, target_path, main_entry_point: entity_to_build.as_path
        logger.info "Detemplatized filed generated in '#{target_path}'"
        target_plugin_name =  entity_to_build.buildable_by
        if target_plugin_name.empty?
          post_build_hook entity_to_build, target_path
        else
          target_plugin = project.plugins[target_plugin_name]
          raise PowerStencil::Error, "Could not find plugin '#{target_plugin_name}' !" if target_plugin.nil?
          raise PowerStencil::Error, "Plugin '#{target_plugin_name}' has no build capability !" unless target_plugin.capabilities[:build]
          target_plugin.plugin_module.send PowerStencil::Plugins::Require::POST_BUILD_HOOK, entity_to_build, target_path
        end
      end

      def post_build_hook(entity_to_build, files_path)
        case entity_to_build.type
        when :simple_exec
          script_to_execute = File.expand_path File.join(files_path, entity_to_build.post_process.process)
          exec_options = {message: "Running '#{script_to_execute}'"}
          exec_options[:show_output] = true if config[:verbose]
          safely_exec_command script_to_execute, exec_options
        end
      end

    end

  end
end
