require 'power_stencil/engine/base'

module PowerStencil
  module Engine

    class InitEngine < PowerStencil::Engine::Base

      def initialize
        super
        root_universe.name = 'Project initialization universe'
        load_system_entities_definition
        load_system_entities
      end

    end

  end
end