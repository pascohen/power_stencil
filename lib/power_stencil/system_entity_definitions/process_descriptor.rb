module PowerStencil
  module SystemEntityDefinitions

    class ProcessDescriptor < PowerStencil::SystemEntityDefinitions::ProjectEntity

      DOC = 'Generic description of a process'

      entity_type :process_descriptor

      field :process, :not_null

    end

  end
end