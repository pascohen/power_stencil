module PowerStencil
  module SystemEntityDefinitions

    class ProjectEntity < UniverseCompiler::Entity::Base

      extend PowerStencil::SystemEntityDefinitions::Buildable
      include PowerStencil::SystemEntityDefinitions::EntityProjectCommon

      entity_type :base_entity

      field :description

    end

  end
end
