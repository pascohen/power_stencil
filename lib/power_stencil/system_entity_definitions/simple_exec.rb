module PowerStencil
  module SystemEntityDefinitions

    class SimpleExec < PowerStencil::SystemEntityDefinitions::ProjectEntity

      include PowerStencil::SystemEntityDefinitions::HasAssociatedFiles

      DOC = 'Describes a simple process to be called after source files have been rendered'.freeze

      entity_type :simple_exec

      buildable_by ''

      has_one :process_descriptor, name: :post_process
      not_null :post_process

      def valid?(raise_error: false)
        unless super(raise_error: false)
          if self.post_process.nil?
            self.post_process = PowerStencil.project.engine.new_entity universe, :process_descriptor, fields: {
                name: "simple_exec_#{name}.process",
                process: './main.sh'
            }
          end
        end
        super(raise_error: raise_error)
      end

      def save(uri = source_uri, force_files_generation: false)
        valid?
        self.post_process.save
        super(uri, force_files_generation: force_files_generation)
      end

      def delete(force_files_deletion: false)
        super
        if self.post_process.name == "simple_exec_#{name}.process"
          self.post_process.delete
        end
        self
      end


    end

  end
end
