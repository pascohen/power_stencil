module PowerStencil
  module SystemEntityDefinitions

    module Buildable

      def buildable
        buildable_by ''
      end

      def buildable_by(plugin_name = nil)
        return @build_plugin_name if plugin_name.nil?
        @build_plugin_name = plugin_name
        self.include PowerStencil::SystemEntityDefinitions::HasAssociatedFiles
      end

      def buildable?
        not @build_plugin_name.nil?
      end

    end

  end
end