module PowerStencil
  module SystemEntityDefinitions

    module HasAssociatedFiles

      def templates_path
        File.join PowerStencil.project.project_root, type.to_s, name
      end

    end

  end
end