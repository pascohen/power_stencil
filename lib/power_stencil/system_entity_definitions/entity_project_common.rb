module PowerStencil
  module SystemEntityDefinitions

    module EntityProjectCommon

      def save(uri = source_uri, force_files_generation: false)
        super(uri)
        unless PowerStencil.project.entity_type_templates[type].nil?
          PowerStencil.project.generate_entity_dir_for_entity self, force: force_files_generation
        end
        self
      end

      def delete(force_files_deletion: false)
        unless PowerStencil.project.entity_type_templates[type].nil?
          PowerStencil.project.delete_entity_dir_for_entity self, force: force_files_deletion
        end
        super()
        self
      end

      def buildable_by
        self.class.buildable_by
      end

      def buildable?
        self.class.buildable?
      end

    end

  end
end
