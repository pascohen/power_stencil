module PowerStencil
  module CommandProcessors

    module TraceHelper

      def log_startup_context
        logger.debug 'Command line manager cmd_line_args: %s' % [command_line_manager.cmd_line_args.inspect]
        logger.debug 'Selected command name: %s' % [command_line_manager.command.name]
        logger.debug 'Selected command cmd_line_args: %s' % [command_line_manager.command.cmd_line_args.inspect]
        logger.debug 'Selected command params_hash: %s' % [command_line_manager.command.params_hash.inspect]
        logger.debug 'Selected processor: %s' % [command_line_manager.processor.inspect]
        logger.debug 'Config is: %s' % [config[].inspect]
        logger.debug 'Logger is: %s' % [logger.inspect]
      end

    end

  end
end

