module PowerStencil
  module CommandProcessors

    class Build

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper



      def execute

        if config[:'supported-builds']
          project.engine.available_entities_hash.select do |_, klass|
            klass.respond_to?(:buildable?) and klass.buildable?
          end .each do |entity_type, _|
            puts " - '#{entity_type}'"
          end
          return
        end

        if config.command_line_layer.extra_parameters.empty?
          raise PowerStencil::Error, 'Please specify something to build'
        end

        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        raise PowerStencil::Error, 'No valid entity specified' if targets.empty?
        project.engine.build targets, fail_on_error: config[:'fail-on-error'], parallelized: config[:parallelized]
        project.engine.last_build_report.each do |entity_full_name, entity_report|
          if entity_report == ['Ok']
            puts " - '#{entity_full_name}' has been correctly built"
          else
            puts " - Failed to build '#{entity_full_name}' because ", entity_report
          end
        end

      end

    end

  end
end