module PowerStencil
  module CommandProcessors

    module EntityHelper

      EntitySearchReference = Struct.new(:type, :name) do
        def as_path
          [type, name].map(&:to_s).join '/'
        end
      end

      def display_entity(entity)
        puts_and_logs "# Entity '#{entity.name}' (#{entity.type})"
        if config[:'names-only']
          puts "- #{entity.name}"
        else
          if config[:raw]
            puts entity.to_yaml
          else
            puts entity.to_hash.to_yaml
          end
        end
      end

      def targets_from_criteria(search_criteria, universe)
        target_universe = if config[:compiled]
                            universe.compile scenario: config[:scenario]
                          else
                            universe
                          end

        if config[:regexp]
          target_universe.get_entities do |entity|
            id = entity.as_path
            keep = false
            search_criteria.each do |criterion|
              if md = id.match(/#{criterion}/i)
                keep = true
                break
              end
            end
            keep
          end
        else
          search_criteria.inject([]) do |res, criterion|
            res.concat target_universe.get_entities(criterion: :by_uniq_key, value: criterion.to_a)
          end
        end
      end

      def extra_params_to_entity_search_criteria(params)
        criteria = []
        next_entity_reference = EntitySearchReference.new
        stack = params.dup
        if config[:regexp]
          criteria = stack
        else
          stack.each do |param|
            if md = param.match(/^\s*(?<type>[^\/]+)\/(?<name>.+)\s*$/)
              next_entity_reference.type = md[:type].to_sym
              next_entity_reference.name = md[:name]
            else
              if next_entity_reference.type.nil? or next_entity_reference.type.empty?
                next_entity_reference.type =  param.to_sym
              else
                next_entity_reference.name = param
              end
            end

            next unless reference_complete? next_entity_reference
            logger.debug "Found entity search pattern in params: #{next_entity_reference.inspect}"
            criteria << next_entity_reference
            next_entity_reference = EntitySearchReference.new
          end
          unless next_entity_reference.type.nil?
            raise PowerStencil::Error, "Invalid parameter '#{next_entity_reference.type}' !"
          end
        end

        criteria
      end

      private


      def analyse_extra_params(extra_params = config.command_line_layer.extra_parameters, default: [''])
        params_to_check = if extra_params.empty? then
                            config[:regexp] = true
                            default
                          else
                            extra_params
                          end
        extra_params_to_entity_search_criteria params_to_check
      end

      def reference_complete?(reference)
        return false if (reference.type.nil? or reference.name.nil?)
        return false if (reference.type.empty? or reference.name.empty?)
        true
      end

    end

  end
end