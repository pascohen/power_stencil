module PowerStencil
  module CommandProcessors

    class Create

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper
      include PowerStencil::Utils::FileEdit

      def execute
        analyse_extra_params.each do |search_criterion|
          begin
            puts_and_logs "Creating new entity '#{search_criterion.as_path}'"
            entity_as_hash = {name: search_criterion.name}
            unless config[:property].nil?
              config[:property].each do |property_def|
                if md = property_def.match(/^\s*(?<prop_name>[^:=]+)\s*[:=]\s*(?<prop_val>.*)\s*$/)
                  logger.debug "Using extra properties from command line '#{md['prop_name']}' = '#{md['prop_val']}'"
                  entity_as_hash[md['prop_name'].to_sym] = md['prop_val']
                else
                  raise PowerStencil::Error, "Invalid command line property definition: '#{property_def}'"
                end
              end
            end

            new_entity = project.engine.new_entity project.engine.root_universe,
                                                   search_criterion.type,
                                                   fields: entity_as_hash,
                                                   user: config[:user]

            if config[:edit]
              logger.debug "Edit mode for entity #{new_entity.to_composite_key} in #{new_entity.source_uri}"
              new_entity = edit_before_save new_entity
            end

            new_entity.valid? raise_error: true
            new_entity.save

            puts "Created '#{new_entity.as_path}'"
          rescue => e
            puts "Failed to create '#{search_criterion.as_path}' with message '#{e.message}'."
            logger.debug PowerStencil::Error.report_error(e)
          end
        end
      end

      private

      def modifications_valid?(modified_path, original_entity)
        test_entity = UniverseCompiler::Entity::Persistence.load modified_path
        test_entity.valid?
      rescue => e
        logger.debug PowerStencil::Error.report_error(e)
        logger.debug "Modifications applied to '#{original_entity.as_path}' are invalid !"
        false
      end

      def edit_before_save(entity)
        securely_edit_entity entity do |modified_path, _|
          modifications_valid? modified_path, entity
        end
      end


    end

  end
end