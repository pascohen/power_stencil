module PowerStencil
  module CommandProcessors

    class NewPlugin

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy

      def execute
        if config.command_line_layer.extra_parameters.empty?
          raise PowerStencil::Error, 'Please specify a plugin name!'
        end
        config.command_line_layer.extra_parameters.each do |plugin_name|
          begin
            target_path = File.join project.project_plugin_path(plugin_name)
            project.create_plugin_tree plugin_name, target_path
            puts "Generated new plugin '#{plugin_name}'"
          rescue => e
            msg = "Could not create plugin '#{plugin_name}' because '#{e.message}'"
            puts msg
            logger.error "Could not create plugin '#{plugin_name}' because '#{e.message}'"
            logger.debug PowerStencil::Error.report_error(e)
          end
        end
      end

    end

  end
end