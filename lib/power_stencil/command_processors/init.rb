require 'power_stencil/engine/init_engine'

module PowerStencil
  module CommandProcessors

    class Init

      include Climatic::Proxy
      include PowerStencil::CommandProcessors::TraceHelper

      def check_params(params)
        true
      end

      def execute
        setup_project_path
        log_startup_context
        puts_and_logs "Creating new project structure in '#{config[:'project-path']}'"
        PowerStencil::Project::Base.create_project_tree config[:'project-path']
        puts_and_logs 'Done.'
      end

      private

      def setup_project_path
        if config[:'project-path']
          unless  config.command_line_layer.extra_parameters.empty?
            raise PowerStencil::Error, 'You can specify the project path only once !'
          end
        else
          if config.command_line_layer.extra_parameters.size > 1
            raise PowerStencil::Error, 'You can specify the project path only once !'
          end
        end
        config[:'project-path'] = if config[:'project-path']
                                    File.expand_path config[:'project-path']
                                  elsif config.command_line_layer.extra_parameters.size == 1
                                    File.expand_path config.command_line_layer.extra_parameters.first
                                  else
                                    File.expand_path Dir.pwd
                                  end
      end

    end

  end
end