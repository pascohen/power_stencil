module PowerStencil
  module CommandProcessors

    class Delete

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper

      def execute
        analyse_extra_params.each do |search_criterion|
          begin
            unless project.engine.entity *search_criterion.to_a, project.engine.root_universe
              puts "Skipping '#{search_criterion.as_path}'. Entity not found."
              next
            end
            puts_and_logs "Deleting entity '#{search_criterion.as_path}'"
            if project.engine.delete_entity project.engine.root_universe,
                                            *search_criterion.to_a,
                                            delete_files: config[:'delete-files']
              msg = "Deleted '#{search_criterion.as_path}'"
              msg << ' and associated files.' if config[:'delete-files']
              puts msg
            else
              puts 'Cancelled by user input.'
            end
          rescue => e
            puts "Failed to delete '#{search_criterion.as_path}' with message '#{e.message}'."
            logger.debug PowerStencil::Error.report_error(e)
          end
        end
      end

    end

  end
end