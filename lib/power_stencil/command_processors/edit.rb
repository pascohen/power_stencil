module PowerStencil
  module CommandProcessors

    class Edit

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper
      include PowerStencil::Utils::FileEdit

      def execute
        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        targets.each do |target|
          securely_edit_file target.source_uri do |modified_path, _|
            modifications_valid? modified_path, target
          end
        end
      end

      private

      def modifications_valid?(modified_path, original_entity)
        test_entity = UniverseCompiler::Entity::Persistence.load modified_path
        test_entity.valid?
      rescue => e
        logger.debug PowerStencil::Error.report_error(e)
        logger.debug "Modifications applied to '#{original_entity.as_path}' are invalid !"
        false
      end

    end

  end
end