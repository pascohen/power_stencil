module PowerStencil
  module CommandProcessors

    class Check

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper

      def execute
        puts 'RAW ENTITIES'
        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        targets.sort {|a,b| a.as_path <=> b.as_path }
            .map do |entity|
          entity_info = [" '#{entity.as_path}':"]
          entity_info << "  - Storage path   : '#{entity.source_uri}'"
          entity_info << "  - Templates path : '#{entity.templates_path}'" if entity.respond_to? :templates_path
          entity_info << "  - Status         : #{entity.valid? ? 'Valid' : 'INVALID !'} "
          entity_info << "  - Buildable      : #{entity.buildable?}"
          entity_info
        end .each { |l| puts l }


        #
        # targets.map do |entity|
        #   " - '#{entity.as_path}': '#{entity.source_uri}'"
        # end .sort.each { |l| puts l }
      end


    end

  end
end