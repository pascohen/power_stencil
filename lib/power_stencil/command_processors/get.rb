module PowerStencil
  module CommandProcessors

    class Get

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy
      include PowerStencil::CommandProcessors::EntityHelper

      def execute
        targets = targets_from_criteria analyse_extra_params, project.engine.root_universe
        targets.each do |target|
          display_entity target
          puts
        end
      end

    end

  end
end