
module PowerStencil
  module CommandProcessors

    class Shell

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy
      include PowerStencil::Project::Proxy

      def execute

        working_universe = if config[:compiled]
                             project.engine.root_universe.compile scenario: config[:scenario]
                           else
                             project.engine.root_universe

                           end

        context = project.engine.running_context working_universe

        require 'pry'

        Pry.hooks.add_hook(:before_session, "startup_message") do
          puts config[:shell_dsl][:session_greetings]
        end

        Pry.start context,
                  prompt: [proc { config[:shell_dsl][:prompt_level_1] }, proc { config[:shell_dsl][:prompt_level_2] }],
                  quiet: true

      end

    end

  end
end