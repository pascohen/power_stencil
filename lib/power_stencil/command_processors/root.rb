module PowerStencil
  module CommandProcessors

    class Root

      include Climatic::Script::UnimplementedProcessor
      include Climatic::Proxy

      def execute
        if config[:version]
          if config[:verbose]
            puts "PowerStencil framework v: #{PowerStencil::VERSION}"
          else
            puts PowerStencil::VERSION
          end
          return
        end
        unless config.command_line_layer.extra_parameters.empty?
          raise PowerStencil::Error, "Invalid command '#{config.command_line_layer.extra_parameters.first}'"
        end
        puts_and_logs 'No action specified. Exiting...'
      end

    end

  end
end