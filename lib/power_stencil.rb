require 'power_stencil/version'

require 'universe_compiler'
require 'dir_glob_ignore'

$DO_NOT_AUTOSTART_CLIMATIC=true
require 'climatic'

require 'power_stencil/error'
require 'power_stencil/utils/semantic_version'
require 'power_stencil/utils/file_helper'
require 'power_stencil/utils/directory_processor'
require 'power_stencil/utils/secure_require'
require 'power_stencil/utils/gem_utils'
require 'power_stencil/utils/file_edit'

require 'climatic/script/unimplemented_processor'
require 'power_stencil/project/proxy'

require 'power_stencil/command_processors/trace_helper'
require 'power_stencil/command_processors/entity_helper'
require 'power_stencil/command_processors/root'
require 'power_stencil/command_processors/init'
require 'power_stencil/command_processors/info'
require 'power_stencil/command_processors/get'
require 'power_stencil/command_processors/check'
require 'power_stencil/command_processors/create'
require 'power_stencil/command_processors/edit'
require 'power_stencil/command_processors/delete'
require 'power_stencil/command_processors/shell'
require 'power_stencil/command_processors/new_plugin'
require 'power_stencil/command_processors/build'


module PowerStencil

  # Module for plugins
  module Plugin; end

end

require 'power_stencil/engine/base'
require 'power_stencil/plugins/base'
require 'power_stencil/project/base'

require 'power_stencil/initializer'


module PowerStencil

  extend PowerStencil::Initializer

end
