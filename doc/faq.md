F.A.Q.
======

<!-- TOC -->

- [How does `PowerStencil` compare with a CMDB ?](#how-does-powerstencil-compare-with-a-cmdb-)
- [Why is a "real" database a bad idea as configuration store for CI/CD ?](#why-is-a-real-database-a-bad-idea-as-configuration-store-for-cicd-)
- [How does `PowerStencil` compare to environment variables + subst ?](#how-does-powerstencil-compare-to-environment-variables--subst-)
- [Why data persisted in YAML ?](#why-data-persisted-in-yaml-)
- [What are the system requirements ?](#what-are-the-system-requirements-)
- [What is the status of `PowerStencil` ?](#what-is-the-status-of-powerstencil-)

<!-- /TOC -->
[:back:][Documentation root]

# How does `PowerStencil` compare with a CMDB ?

It could sound like a strange question. `PowerStencil` is absolutelly not designed with ITIL in mind but in the end its true that its data management part looks a lot like a [CMDB].

- [Entities][entities] (and more specifically entity types) can cover most aspects (I am not ITIL expert enough to say it covers all of them) of ITIL's _Configuration Items_. Default entities provided with `PowerStencil` do not contain all necessary attributes to do be really considered as _configuration items_, but it should be very easy to achieve with custom entities. In fact it could be a good idea of [plugin][plugins]...
- Relation semantics in `PowerStencil` cover 1-1, 1-n and composition relation patterns.
- Versioning is achieved through Git. Implying on top of versioning, branching, merging

But `PowerStencil` cannot be simplified to it's "database" aspect. It goes well beyond, and is as well a pragmatic workflow for developers and operations to actually do something out of this data. A [CMDB] may be used by external processes in order to achieve what `PowerStencil` does out of the box.


# Why is a "real" database a bad idea as configuration store for CI/CD ?

There are some reasons why using a database in the scope of CI/CD is not a good idea.

Basically the CI/CD infrastructure could be built around Jenkins, containerized tools (Drone for example), Travis, GitlabCI... The goal being to provide the **easiest possible way for the developer** to perform integration tests and eventually build and deploy software.

But the easiest possible way for the developer doesn't mean that the underlying infrastructure is simple to put in place or maintain. In fact it is not !

Anything you add to this carefully crafted mechanic could turn into an infrastructure management nightmare. And a database (and its availability, maintenance) is clearly not the simplest component to inject in your infrastructure...

On top of this, a database does not not provide by default all the required features. Versioning is obviously one of them but of course not the only one (you may think about branching or others...). Meaning that **you need to have custom developments** on top of the chosen database in order to achieve all the required features. It immediately becomes a de-facto single point of failure in your CI/CD process...

Relying on Git to achieve the storage, versioning, branching, decentralization is a much easier way to achieve the same. There is no need to have specific developments as any CI/CD mechanism knows how to deal with extra git repository.

# How does `PowerStencil` compare to environment variables + subst ?

If you have very few files/templates, you may consider this as a solution, but as soon as your templates will complexify or some data need to reference some other, you will enter a loop of death that will make you regret your initial choice.

`PowerStencil` is not a templating engine, it is a framework! 

Not only it [allows structured and relational data][entities], it structures your work as well, and provides mechanisms to allow developers to [easily perform tests without committing unwanted changes by mistake][overrides].

Due on how it is easy to use `PowerStencil`, even for the simplest use cases, there is no real reason not to use it.


# Why data persisted in YAML ?

Assuming Git is used for storage and versioning **we need to have a data format that is git-friendly**.

- So it discards local databases like SQLite, BerkeleyDB or others which are binary formats.
- XML is **not** a human readable format and is a real pain to _diff_. It should have been kept for what it has been designed for, ie machine to machine protocols...

So the two main remaining contenders are JSON and YAML. Although there is no real reason to take one more than the other, including the manipulating language, would I have written `PowerStencil` in JS or Go, I may probably have chosen JSON as storage format. But it is written in Ruby and therefore I went for YAML.


# What are the system requirements ?

`PowerStencil` has been developed under Linux, but it's only requiring a working Ruby installed on your machine. 

As such, even if I never tested, I would expect it to probably work out of the box on MacOS, BSD or other Unices supporting Ruby 2.2+.

Regarding Windows, I stopped using spywareOS years ago so I can't really tell you. Last time I tried using Ruby on Windows (maybe 10 years ago), I did face a lot of dependency issues. Maybe things are better now, but I have no idea. As far as I understood you can now have an Ubuntu running on top of Windows 10 as a kind of blessed VM. I would probably more go this way...


# What is the status of `PowerStencil` ?

Although `PowerStencil` is still under development, **it can already be safely used**.

Regarding `PowerStencil` core:

- The part that may evolve the most is the plugin part (including documentation which is not really in par with the rest...), but it should not break anything developed now.
- New system entity types may appear. 

Anyway `PowerStencil` follows the semantic versioning pattern and if incompatible changes arise it may be reflected in the version.

There are already some plugins in the pipe, especially around contenerization topics.

:information_source: Of course, `PowerStencil` is new software and it is expected to evolve with the needs of its users...




[:back:][Documentation root]
<!-- End of Document -->

<!-- Pages -->
[Documentation root]: ../README.md "Back to documentation root"
[entities]: entities.md "Entities in PowerStencil"
[plugins]: plugins.md "Plugins in PowerStencil"
[overrides]: builds.md#overriding-entities-and-build-scenarii "Overriding data locally"


<!-- Code links -->


<!-- Illustrations -->


<!-- External links -->
[CMDB]: https://en.wikipedia.org/wiki/Configuration_management_database "Configuration Management Database"
