Example use cases
=================

<!-- TOC -->

- [Overview](#overview)
- [Managing machines, routers, firewalls...](#managing-machines-routers-firewalls)
- [Deploying Snaps, Docker containers, VMs](#deploying-snaps-docker-containers-vms)
- [And...](#and)

<!-- /TOC -->
[:back:][Documentation root]


# Overview

Considering the genericity of the `PowerStencil` framework, it is a bit difficult to define a strict scope for its usage. It's a bit like wondering what you could do with a programming language.

Neverthless, I will give two short ideas that should open you mind to things you could do with it.


# Managing machines, routers, firewalls...

If you are in a network, where you manage let's say a DCHP server, a DNS and a firewall, you have probably to deal with a number of config files with a lot of information in common but very different file formats.

This is exactly where the need of an agnostic data storage and multiple generated config files would be very useful.

You could define machines, interfaces, networks as [entities] and having [templates] for each of the maintained config files.

And of course, changes are tracked using Git, providing easy fallback for something as critical as your network config.

# Deploying Snaps, Docker containers, VMs

If you manage a containerized infrastructure, you probably noticed how often you need to have the same information duplicated from your application builds, Snaps, Docker containers or VM definitions (names, ports, entry-points...)

This is where the paradigm of `PowerStencil` will ensure constency across your various deployment units. You can integrate this in your CI/CD infrastructure, avoiding the painful communication with third party database while maintaining traceability.


# And...

There are many example we could think about, now it's your turn ! [Read the doc], try it and discover what `PowerStencil` can do for you.

[:back:][Documentation root]
<!-- End of Document -->

<!-- Pages -->
[Documentation root]: ../README.md "Back to documentation root"
[templates]: templates.md "Templates in PowerStencil"
[entities]: entities.md "Entities in PowerStencil"
[Read the doc]: ../README.md#getting-started "Get started with PowerStencil"


<!-- Code links -->


<!-- Illustrations -->


<!-- External links -->