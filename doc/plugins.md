Plugins
=======

<!-- TOC -->

- [Why plugins ?](#why-plugins-)
    - [Capitalize on your custom entity types](#capitalize-on-your-custom-entity-types)
    - [Functional code growing](#functional-code-growing)
    - [Need of very specific post actions developed in Ruby](#need-of-very-specific-post-actions-developed-in-ruby)
    - [Enhance `power_stencil` command line](#enhance-power_stencil-command-line)
- [What are plugins ?](#what-are-plugins-)
- [Creating plugin local to the project](#creating-plugin-local-to-the-project)

<!-- /TOC -->
[:back:][Documentation root]

# Why plugins ?

If you read everything about [entities], [templates] and [builds], you may wonder why you may ever need to create plugins. There are nevertheless good reasons to build real plugins.

## Capitalize on your custom entity types

You have designed super nicely crafted entity types that you may want to **re-use** in other projects.

## Functional code growing

If you start developing a lot of code around your entity types, it may become a bad idea to keep everything within your entity types code and you may want to **structure** your code, add tests to it etc...

## Need of very specific post actions developed in Ruby

If calling an executable after the [build][builds] process completed is not enough and you want some custom Ruby to be run right after the [build][builds] completion.

## Enhance `power_stencil` command line

If you want to have new custom sub-commands or options available.


# What are plugins ?

Plugins are actually Ruby Gems with a specific structure. Plugins can be part of the project or provided as a separated stand-alone Gem.

The normal process would be to begin with a plugin within the project and once you're ok with its features you may release it as a standalone Ruby Gem.

- Plugins local to the project are automatically taken in account.
- To use plugins provided as Gems you have to set the `:plugins:` array property in the `.ps_project/versioned-config.yaml`

:warning: Plugins provided as standalone Gems are not thoroughly tested, for the time being you may use plugins local to the project.


# Creating plugin local to the project

The is a command provided for that, that will create a whole plugin skeleton.

```shell
$ power_stencil new-plugin myplugin
Generated new plugin 'myplugin'

$ ll .ps_project/plugins/myplugin
total 60
drwxrwxr-x 6 laurent laurent 4096 août  28 14:38 ./
drwxrwxr-x 3 laurent laurent 4096 août  28 14:38 ../
drwxrwxr-x 2 laurent laurent 4096 août  28 14:38 bin/
-rw-rw-r-- 1 laurent laurent 3228 août  28 14:38 CODE_OF_CONDUCT.md
drwxrwxr-x 3 laurent laurent 4096 août  28 14:38 etc/
-rw-rw-r-- 1 laurent laurent  163 août  28 14:38 Gemfile
-rw-rw-r-- 1 laurent laurent  113 août  28 14:38 .gitignore
drwxrwxr-x 3 laurent laurent 4096 août  28 14:38 lib/
-rw-rw-r-- 1 laurent laurent 1077 août  28 14:38 LICENSE.txt
-rw-rw-r-- 1 laurent laurent 1491 août  28 14:38 psplugin_myplugin.gemspec
-rw-rw-r-- 1 laurent laurent  117 août  28 14:38 Rakefile
-rw-rw-r-- 1 laurent laurent 1770 août  28 14:38 README.md
-rw-rw-r-- 1 laurent laurent   53 août  28 14:38 .rspec
drwxrwxr-x 2 laurent laurent 4096 août  28 14:38 spec/
-rw-rw-r-- 1 laurent laurent   88 août  28 14:38 .travis.yml
```

You can see that it looks a lot like a Ruby Gem, and it's normal because it is ;-)...

So what was the impact on the project.

If you do a `power_stencil info` you see a new part appeared in the report

```
--------------------------------------------------------------------------------
 Plugins:
 --> Plugin 'myplugin' has following capabilities:
   - command_line: true
   - local_config: true
   - processors: true
   - build: false
   - dsl: false
   - entity_definitions: true
   - code: true
   - version: true
   - templates: true
```
Each of the lines correspond to what is called a _plugin capability_.

Obviously by default the plugin does nothing, but it defined some placeholders. For example in the ouput here above it says `command_line: true`. What could it mean ?

Let's try to see to help:

```shell
$ power_stencil --help
PowerStencil is the Swiss-army knife templating workflow for developers and ops.
-- Options ---------------------------------------------------------------------
    -v, --verbose  Displays extra runtime information.
    -h, --help  Displays this help.
    --program-version, -V, --version  Displays program version.
    --simulate  Will not perform actual actions
    --debug  Debug mode
    --debug-on-err, --debug-on-stderr  Sends debugging to SDTERR
    --log-level  Defines the level of logging (0 to 5)
    --log-file  Specifies a file to log into
    --truncate-log-file  Truncates the log file (appends by default)
    --project-path  Specifies a startup path to use instead of '.'
    --auto  Bypasses command-line confirmations to the user

--------------------------------------------------------------------------------
 Following subcommands exist too:
 For more information you can always issue sub_command_name --help...
--------------------------------------------------------------------------------
 * init: Initializes a PowerStencil repository ...
 * info: Generic information about the repository ...
 * new-plugin: Generates the skeleton for a plugin ...
 * get: Query entities from repository ...
 * shell: Opens a shell to interact with entities ...
 * check: Check repository entities consistency ...
 * create: Creates entities in the repository ...
 * edit: Edit entities from repository ...
 * delete: Delete entities from repository ...
 * build: Builds entities ...
 * myplugin: Does nothing useful Added by plugin myplugin
```

You can see that the plugin brought a new sub-command `myplugin`.

Let's try it:

```shell
$ power_stencil myplugin
MYPLUGIN PLUGIN WAZ HERE
```

Wow it worked ! Useless, but worked !


The documentation for plugins is not yet done, so you are encouraged to read the code, the capabilities (in the output of `power_stencil info`) give you an idea of what plugins can do.

:warning: As opposed to the rest of `PowerStencil`, the functionnality is nevertheless not completely frozen.


[:back:][Documentation root]
<!-- End of Document -->

<!-- Pages -->
[Documentation root]: ../README.md "Back to documentation root"
[entities]: entities.md "Entities in PowerStencil"
[builds]: builds.md "Builds in PowerStencil"
[templates]: templates.md "Templates in PowerStencil"


<!-- Code links -->


<!-- Illustrations -->


<!-- External links -->