PowerStencil
============

`Power Stencil` is the Swiss-army knife templating workflow for developers and ops.

<!-- TOC -->

- [Overview](#overview)
- [Pfff, yet another templating engine...](#pfff-yet-another-templating-engine)
- [Installation](#installation)
- [Usage](#usage)
    - [Help](#help)
    - [Creating a `PowerStencil` project](#creating-a-powerstencil-project)
    - [`PowerStencil` project structure](#powerstencil-project-structure)
    - [Getting started](#getting-started)
- [Contributing](#contributing)
    - [License](#license)
    - [Code of Conduct](#code-of-conduct)

<!-- /TOC -->

# Overview

`PowerStencil` proposes a radical approach on how to manage your shared configuration.

Configuration is one of the most complex things to maintain, and anyone who participated in a large scale piece of software or documentation knows how complex it is to maintain, keep consistent and avoid duplication on the long run across the various projects that may share this configuration.

From a very high level point of view, the workfow `PowerStencil` proposes looks like:

![simple-flow-image]

`PowerStencil` provides **development and operations friendly workflows to fully manage the maintenance of a complex shared config** in order to generate anything you may want like _documentation_, _static sites_, _code_, _configuration_ for multiple tools..., while **avoiding duplication, and manage it like code** !

It is a common pattern to introduce a database to manage shared configuration for CI/CD processes, and it is a very bad idea (see [F.A.Q.]) because it implies extra tooling, and specific developments that de-facto become single point of failure at the heart of your development process. Why would you do that when something as versatile and ubiquitous as Git exists that will integrate smoothly within your existing development and operations workflows, builds, CI/CD processes, while benefiting from all the goodness provided by Git, like branches, decentralization, versioning, tags, code reviews, traceability, simplified fallbacks etc... ?

A `PowerStencil` project is composed of a data repository and a pretty standard templating flow to generate whatever is needed by your project or organization and **with `PowerStencil` everything is maintained in a git repository**: data, templates, specific code **without sacrificing on data integrity and relational features**. No more config loss, no more change applied without a full lineage.

The `PowerStencil` gem provides the `power_stencil` executable which is a pluggable CLI to:

- Manage the project data
- Manage templates
- Manage builds
- Create and manipulate plugins

# Pfff, yet another templating engine...

**Actually `PowerStencil` is _not_ a templating engine** per se. Instead it uses already existing and proven templating engines.
Currently it uses [ERB], which is pretty standard in [Ruby] world, but this is not hardcoded and any templating engine could be used (in parallel, meaning different templating engines can be used automatically depending on what you actually want to generate), even if only [ERB] is implemented for the time-being, `PowerStencil` is modular from ground-up and at least a second implementation for [Haml] templating engine is planned to ease generation of XML-like files, still currently you can do it with [ERB]. See the [templates] section for more information about templates in `PowerStencil`.

No, the real value of `PowerStencil` is not really in its templating capabilities even if very powerful, but in the development flow it proposes around this templating mechanism for both development and/or operations teams, in the way you organize and trace the data which is the real asset, and in the way you can easily extend it to fit the particular needs of a project or organization.

# Installation

You need a working install of the [Ruby] language (>= 2.2).

You can install the `PowerStencil` gem by issuing the usual:

    $ gem install power_stencil

# Usage

## Help

The `power_stencil` CLI provides a contextual help. You can start by:

    $ power_stencil --help

That will display a basic help:

```shell
PowerStencil is the Swiss-army knife templating workflow for developers and ops.
-- Options ---------------------------------------------------------------------
    -v, --verbose  Displays extra runtime information.
    -h, --help  Displays this help.
    --program-version, -V, --version  Displays program version.
    --simulate  Will not perform actual actions
    --debug  Debug mode
    --debug-on-err, --debug-on-stderr  Sends debugging to SDTERR
    --log-level  Defines the level of logging (0 to 5)
    --log-file  Specifies a file to log into
    --truncate-log-file  Truncates the log file (appends by default)
    --project-path  Specifies a startup path to use instead of '.'
    --auto  Bypasses command-line confirmations to the user

--------------------------------------------------------------------------------
 Following subcommands exist too:
 For more information you can always issue sub_command_name --help...
--------------------------------------------------------------------------------
 * init: Initializes a PowerStencil repository ...
 * info: Generic information about the repository ...
 * new-plugin: Generates the skeleton for a plugin ...
 * get: Query entities from repository ...
 * shell: Opens a shell to interact with entities ...
 * check: Check repository entities consistency ...
 * create: Creates entities in the repository ...
 * edit: Edit entities from repository ...
 * delete: Delete entities from repository ...
 * build: Builds entities ...
```

The program uses the standard paradigm of sub-commands (à-la-git), and each can have its own options that you can check using:

    $ power_stencil <subcommand> --help

[Plugins] may bring extra subcommands and/or options, so depending on the project you are working in, the output of `--help` may differ...

## Creating a `PowerStencil` project

To create a new project, use the `init` sub-command. It works as you would expect. If you run it in an existing directory it will create the project here but you can specify the directory where you want the project to be created (the path will be created provided you have the rights on the filesystem):

    $ power_stencil init --project-path /where/you/want

The `--project-path` option can be applied to any sub-command, but à-la-git, once the project created if you are anywhere within the project tree, you don't need to specify the project path.

:information_source: The rest of this documentation will assume you are at the root of this created project.

:information_source: Of course, once you created a new project you should version it with Git:

    $ git init
    $ git commit -a -m 'Initial commit'


## `PowerStencil` project structure

The structure of a brand new `PowerStencil` project is the following:

```
├── .gitignore
└── .ps_project
    ├── entities
    │   └── README.md
    ├── entity_definitions
    │   └── README.md
    ├── personal-config.yaml
    ├── plugins
    ├── templates-templates
    │   └── README.md
    ├── user_entities
    │   └── README.md
    └── versioned-config.yaml
```
New directories will appear once you start some [builds] or when you will define [templates].

Not all the files in this directory should be versioned, and this the reason why there is a preconfigured `.gitignore` at the root of the project.

The project has two distinct config files. Keep the official project config in the `.ps_project/versioned-config.yaml` which, as its name suggest will be versioned. But any developer who wants to temporarily override some configuration should do it in the `.ps_project/personal-config.yaml` which is not be versioned. The content defined in the latter overrides the content of the official one. This is a clean way for anyone who wants to test something to safely do it locally without the risk to pollute the central repo.

There is the same mechanism between `.ps_project/entities` and `.ps_project/user_entities`, but as opposed to the two aforementioned config files you should not edit anything directly there, but instead interact using the CLI. More information on how to manage entities [here][entities].

The `plugins` directory can optionally contain project specific plugins. Plugins are a great way to extend `PowerStencil` for the needs of a project or organization. See the [plugins] documentation.

## Getting started

The core of the system is the `entity`, so you should start by having a look at what entities are, how they are easily managed using the `PowerStencil` CLI and shell.

> :arrow_forward: [Entities]

Then it is important to understand how to use entities within `templates`. Templates are the way to generate something (doc, code, descriptors, sites, whatever...) from the data that the entities represent.

> :arrow_forward: [Templates]

The mechanism that combines entities and templates is called a `build`. A build is always attached to an entity (you build something). The result of a build is a set of files. Optionally an action can be triggered after the files are generated (could be as simple as calling a script that has been generated).

> :arrow_forward: [Builds]

`PowerStencil` could stop there, and you would be able to do whatever you want, but there is a whole world beyond. `Plugins` provide a way to completely extend `PowerStencil`, further control relations between entities, implement complex post-build actions, add CLI sub-commands and options. Plugins can be local to the project or coming in the form of standard Ruby Gems ! The icing on the cake...

> :arrow_forward: [Plugins]

And course, you may:

- Read the [F.A.Q.]
- See some real world [example use cases].


# Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/tools4devops/power_stencil. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License].

## Code of Conduct

Everyone interacting in the PowerStencil project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct].

<!-- End of Document -->

<!-- Pages -->
[templates]: doc/templates.md "Templates in PowerStencil"
[entities]: doc/entities.md "Entities in PowerStencil"
[builds]: doc/builds.md "Builds in PowerStencil"
[plugins]: doc/plugins.md "Plugins in PowerStencil"
[example use cases]: doc/example_use_cases.md "Example uses cases using PowerStencil"
[F.A.Q.]: doc/faq.md "PowerStencil F.A.Q."
[code of conduct]: CODE_OF_CONDUCT.md

<!-- Code links -->
[ERB implementation]: lib/power_stencil/engine/renderers/erb.rb "Mostly the only module you need to clone to implement another templating engine"

<!-- Illustrations -->
[simple-flow-image]: doc/images/power-stencil-simple-flow.svg

<!-- External links -->
[MIT License]: http://opensource.org/licenses/MIT "The MIT license"
[ERB]: https://ruby-doc.org/stdlib-2.6.3/libdoc/erb/rdoc/ERB.html "Quick ERB description"
[Haml]: http://haml.info/ "The templating engine for XML-like documents"
[Ruby]: https://www.ruby-lang.org "The powerful Ruby language"