PowerStencil entities
=====================


In this directory, you will find all project entities.

They are organized as `<entity_type>/<entity_name>.yaml`

__It is strongly advised to keep this directory under source control__

This should be the default PowerStencil behaviour. 