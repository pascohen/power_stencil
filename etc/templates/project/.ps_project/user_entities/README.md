PowerStencil user entities
==========================


In this directory, you will find all project entities locally defined by 
the user (overrides but also entities not meant to be versioned)

They are organized as `<entity_type>/<entity_name>.yaml`

__It is strongly advised NOT to keep this directory under source control__

This should be the default PowerStencil behaviour. 